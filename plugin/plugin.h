// plugin.h

#ifndef PLUGINXXX_H
#define PLUGINXXX_H

typedef struct GtkWidget GtkWidget_;  

typedef GtkWidget_ *(*GetPlugin)();
typedef GtkWidget_ *(*GetTitle)();

#define EXPORT_PLUGIN(p, t) \
     GtkWidget *getPlugin () \
     {return p();} \
     GtkWidget *getTitle () \
     {return t();}
   
#endif
