// main.c

#include "plugin.h"
#include <gtk/gtk.h>
#include <gmodule.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct 
{
    GtkWidget *page;
    GtkWidget *title;
} NotePage;

GList *loadPlugins()
{
    GList     *plugins;
    GModule   *module;
    FILE      *fp;
    GetPlugin  getPlugin;
    GetTitle   getTitle;
    char       buf[1024];
    int        n;
    
    plugins = NULL;
    module  = NULL;
    fp      = NULL;
    
    memset(buf, 0, 1024);
    fp = popen("sh -c \"ls /home/x/tmp/learn/gmodule/plugin/build/*.so\"", "r");
    
    while (fgets(buf, 1024, fp) != NULL)
    {
        n = strlen(buf) - 1;
        if('\n' == buf[n])
            buf[n] = '\0';

        printf ("%s\r\n", buf);

        module = g_module_open (buf, G_MODULE_BIND_LAZY);
        if (module)
        {
            if (g_module_symbol (module, "getPlugin", (gpointer *)&getPlugin))
                g_print ("Load plugin ok\n");
            if (g_module_symbol (module, "getTitle", (gpointer *)&getTitle))
                g_print ("Load title ok\n");
    
	    NotePage *page = malloc (sizeof(NotePage));
	    page->page = GTK_WIDGET (getPlugin());
	    page->title = GTK_WIDGET (getTitle());
	    
	    plugins = g_list_append (plugins, page);
        }
    }

    return plugins;
}

int main(int argc, char *argv[])
{
    GtkWidget *window;
    GtkWidget *notebook;

    gtk_init (&argc, &argv);
    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    g_signal_connect (window, "destroy",
                      G_CALLBACK (gtk_main_quit), NULL);

    notebook = gtk_notebook_new ();
    gtk_notebook_append_page (GTK_NOTEBOOK (notebook), gtk_label_new ("A page"),
                              gtk_label_new ("page1"));
    gtk_notebook_append_page (GTK_NOTEBOOK (notebook), gtk_label_new ("A page"),
                              gtk_label_new ("page2"));
    gtk_container_add (GTK_CONTAINER (window), notebook);

    GList *l = loadPlugins();
    for (; l != NULL; l = l->next)
    {
       NotePage *page = l->data;
       gtk_notebook_append_page (GTK_NOTEBOOK (notebook), page->page, page->title);
    }

    gtk_widget_show_all (window);

    gtk_main ();
    return 0;
}
